//1. Two Sum----------------------------------------------------------------------------------------------
//Given an array of integers, return indices of the two numbers such that they add up to a specific target.
//You may assume that each input would have exactly one solution, and you may not use the same element twice.

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
    for(var i = 0; i<nums.length; i++){
        for(var j = i+1; j<nums.length; j++){
            if(i===j)
                continue;
            if((nums[i]+nums[j]) === target){
                return [i, j];
            }
        }
    }
};


//2. Add Two Numbers-------------------------------------------------------------------------------------
//You are given two non-empty linked lists representing two non-negative integers.
//The digits are stored in reverse order and each of their nodes contain a single digit.
//Add the two numbers and return it as a linked list.
//You may assume the two numbers do not contain any leading zero, except the number 0 itself.

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function(l1, l2) {
    var tempL1 = l1;
    var tempL2 = l2;
    var originalHead = new ListNode(0);
    var tempHead = originalHead;
    var carry = false;
    var x,y;

    while(!(tempL1 == null && tempL2 == null)){
        if(tempHead.next != null)
            tempHead = tempHead.next;

        x = tempL1 != null ? tempL1.val : 0;
        y = tempL2 != null ? tempL2.val : 0;

        var sum = carry ? x+y+1 : x+y;

        if(sum >= 10){
            carry = true;
            tempHead.val = sum%10;
        } else{
            carry = false;
            tempHead.val = sum;
        }

        var newNode = new ListNode(0);
        tempHead.next = newNode;

        tempL1 = tempL1 != null ? tempL1.next : null;
        tempL2 = tempL2 != null ? tempL2.next : null;
    }

    if(carry){
        tempHead.next.val = 1;
    } else{
        tempHead.next = null;
    }

    return originalHead;
};



//3. Longest Substring Without Repeating Characters----------------------------------------
//Given a string, find the length of the longest substring without repeating characters.

/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {
    var letters = new Set();
    var tempLongest = 0;
    for(var i = 0, j = 0; i<s.length && j<s.length;){
        if(!letters.has(s[j])){
            letters.add(s[j++]);
            tempLongest = Math.max(tempLongest, j-i);
        } else{
            letters.delete(s[i++]);
        }

    }
    return tempLongest;
};
